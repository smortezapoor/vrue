﻿using System;
using UnityEngine;
using System.Collections;

// TODO: define the behaviour of a shared object when it is manipulated by a client

public class OnGrabbedBehaviour : MonoBehaviour {

   

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        
        
	}

    // called first time when the GO gets grabbed by a player
    public void OnGrabbed()
    {
       if (LocalPlayerController.Singleton)
       {
           Transform left = LocalPlayerController.Singleton.actor.character.left.GetChild(0).transform;
           Transform right = LocalPlayerController.Singleton.actor.character.right.GetChild(0).transform;
           this.transform.position = (left.position + right.position) / 2;
           float handsXDistance = (left.position.x - right.position.x);
           float handsZDistance = (left.position.z - right.position.z);
           float handsYDistance = (left.position.y - right.position.y);
           float angleOfTheCubeY_Z =
               (float)(Math.Atan(handsYDistance / handsZDistance) * (180.0 / Math.PI));
           float angleOfTheCubeX_Z =
               (float)(Math.Atan(handsXDistance / handsZDistance) * (180.0 / Math.PI));

            this.transform.eulerAngles = new Vector3(-angleOfTheCubeY_Z, angleOfTheCubeX_Z, 0);
        }
    }

    // called when the GO gets released by a player
    public void OnReleased()
    {
        

    }



    private ConfigurableJoint configurableJoint;

}
