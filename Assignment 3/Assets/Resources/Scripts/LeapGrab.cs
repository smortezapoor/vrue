﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

// This script defines conditions that are necessary for the Leap player to grab a shared object
// TODO: values of these four boolean variables can be changed either directly here or through other components
// AuthorityManager of a shared object should be notifyed from this script

public class LeapGrab : MonoBehaviour {

    [SerializeField] AuthorityManager am; // to communicate the fulfillment of grabbing conditions
    private CollidedObjectAuthorityManagerAggregator collidedObjectAuthorityManagerAggregator;

    // conditions for the object control here
    bool leftHandTouching = false;
    bool rightHandTouching = false;
    bool leftPinch = false;
    bool rightPinch = false;



    #region LEAPActions
    public void OnRightPinchDetected()
    {
        rightPinch = true;
    }

    public void OnLeftPinchDetected()
    {
        leftPinch = true;
    }


    public void OnRightPinchCanceled()
    {
        rightPinch = false;
    }

    public void OnLeftPinchCanceled()
    {
        leftPinch = false;
    }
    #endregion

    // Use this for initialization
    void Start()
    {
        collidedObjectAuthorityManagerAggregator = LocalPlayerController.Singleton.actor.GetComponent<CollidedObjectAuthorityManagerAggregator>();
    }

    // Update is called once per frame
    void Update()
    {
        AuthorityManager collidedAuthorityManager = collidedObjectAuthorityManagerAggregator.collidedAuthorityManager;

        leftHandTouching = collidedObjectAuthorityManagerAggregator.IsLeftHandTouching;
        rightHandTouching = collidedObjectAuthorityManagerAggregator.IsRightHandTouching;
        bool grabCriteria = leftHandTouching && rightHandTouching && leftPinch && rightPinch;


        //if we have a box nearby
        if (collidedAuthorityManager != null)
        {
            //if this box is not the box that was nearby recently
            if (am != collidedAuthorityManager)
            {
                //if there is no former box or if the former box is not grabbed
                if (am is null || !am.grabbedByPlayer)
                {
                    am = collidedAuthorityManager;
                }
            }
        }

        bool isAuthorityManagerAttached = !(am is null);
        if (grabCriteria && isAuthorityManagerAttached)
        {
            am.grabbedByPlayer = true;
        }
        else if (isAuthorityManagerAttached)
        {
            am.grabbedByPlayer = false;

            if (collidedAuthorityManager == null)
            {
                am = null;
            }
        }




    }
}
