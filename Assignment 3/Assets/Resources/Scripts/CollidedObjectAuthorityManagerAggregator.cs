﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class CollidedObjectAuthorityManagerAggregator : NetworkBehaviour
{
    [SerializeField]
    private CollidedObjectAuthorityManagerFinder[] collidedObjectAuthorityManagerFinders;


    public bool IsRightHandTouching
    {
        get
        {
            CheckAndInitialize();
            if(collidedObjectAuthorityManagerFinders.Any())
            {
                CollidedObjectAuthorityManagerFinder collidedObjectAuthorityManagerFinder =
                    collidedObjectAuthorityManagerFinders.Single(f => f.transform.name.Contains("Left"));
                return collidedObjectAuthorityManagerFinder.collidedAuthorityManager != null;
            }
            return true;
        }
    }


    public bool IsLeftHandTouching
    {
        get
        {
            CheckAndInitialize();
            if (collidedObjectAuthorityManagerFinders.Any())
            {
                CollidedObjectAuthorityManagerFinder collidedObjectAuthorityManagerFinder =
                    collidedObjectAuthorityManagerFinders.Single(f => f.transform.name.Contains("Right"));
                return collidedObjectAuthorityManagerFinder.collidedAuthorityManager != null;
            }
            return true;
        }
    }

    public AuthorityManager collidedAuthorityManager
    {
        get
        {
            CheckAndInitialize();

            IEnumerable<AuthorityManager> authorityManagers = collidedObjectAuthorityManagerFinders
                .Where(a => a.collidedAuthorityManager != null)
                .Select(a => a.collidedAuthorityManager);

            if (!authorityManagers.Any())
                return null;
            else
            {
                return authorityManagers.First();
            }
        }

    }

    private void CheckAndInitialize()
    {
        if (!collidedObjectAuthorityManagerFinders.Where(c => !(c is null)).Any())
        {
            collidedObjectAuthorityManagerFinders = LocalPlayerController.Singleton
                .GetComponentsInChildren<CollidedObjectAuthorityManagerFinder>();
        }
    }


    // Start is called before the first frame update
    void Start()
    {
            CheckAndInitialize();
    }

    // Update is called once per frame
    void Update()
    {
         
    }
}
