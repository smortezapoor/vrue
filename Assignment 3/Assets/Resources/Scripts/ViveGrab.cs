﻿using UnityEngine;
using System.Collections;
using System.Linq;
using Valve.VR;

// This script defines conditions that are necessary for the Vive player to grab a shared object
// TODO: values of these four boolean variables can be changed either directly here or through other components
// AuthorityManager of a shared object should be notifyed from this script

public class ViveGrab : MonoBehaviour {

    //am should be assigned based on collision to an object with AM
    [SerializeField] AuthorityManager am; // to communicate the fulfillment of grabbing conditions
    private CollidedObjectAuthorityManagerAggregator collidedObjectAuthorityManagerAggregator;

    private SteamVR_Behaviour_Boolean[] triggers;


    // conditions for the object control here
    bool leftHandTouching = false;
    bool rightHandTouching = false;
    bool leftTriggerDown = false;
    bool rightTriggerDown = false;

    #region ViveActions
    public void OnRightTriggerDown()
    {
        rightTriggerDown = true;
    }

    public void OnLeftTriggerDown()
    {
        leftTriggerDown = true;
    }


    public void OnRightTriggerUp()
    {
        rightTriggerDown = false;
    }

    public void OnLeftTriggerUp()
    {
        leftTriggerDown = false;
    }
    #endregion

    // Use this for initialization
    void Start () {
        collidedObjectAuthorityManagerAggregator = LocalPlayerController.Singleton.actor.GetComponent<CollidedObjectAuthorityManagerAggregator>();
        triggers = GetComponents<SteamVR_Behaviour_Boolean>();
    }
	
	// Update is called once per frame
    void Update()
    {
        AuthorityManager collidedAuthorityManager = collidedObjectAuthorityManagerAggregator.collidedAuthorityManager;

        leftHandTouching = collidedObjectAuthorityManagerAggregator.IsLeftHandTouching;
        rightHandTouching = collidedObjectAuthorityManagerAggregator.IsRightHandTouching;
        bool grabCriteria = leftHandTouching && rightHandTouching && leftTriggerDown && rightTriggerDown;


        //if we have a box nearby
        if (collidedAuthorityManager != null)
        {
            //if this box is not the box that was nearby recently
            if (am != collidedAuthorityManager)
            {
                //if there is no former box or if the former box is not grabbed
                if (am is null || !am.grabbedByPlayer)
                {
                    am = collidedAuthorityManager;
                }
            }
        }

        bool isAuthorityManagerAttached = !(am is null);
        if (grabCriteria && isAuthorityManagerAttached)
        {
            am.grabbedByPlayer = true;
        }
        else if (isAuthorityManagerAttached)
        {
            am.grabbedByPlayer = false;

            if (collidedAuthorityManager == null)
            {
                am = null;
            }
        }




    }
}
