﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class CollidedObjectAuthorityManagerFinder : MonoBehaviour
{

    public AuthorityManager collidedAuthorityManager = null;
    private bool collided = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "shared")
        {
            this.collided = true;
            this.collidedAuthorityManager = ClientScene.FindLocalObject(other.GetComponent<NetworkIdentity>().netId)
                                            .GetComponent<AuthorityManager>();
        }
    }

    private void OnTriggerExit(Collider other)
    {

        this.collided = false;
    }


    private IEnumerator SetColliderToNone()
    {
        yield return new WaitForSeconds(1f);
        if(!collided)
            collidedAuthorityManager = null;
    }
}
