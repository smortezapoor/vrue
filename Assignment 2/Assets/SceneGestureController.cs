﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valve.VR;

public class SceneGestureController : MonoBehaviour
{

    

    public SteamVR_Action_Boolean steamVrActionBoolean;
    [SerializeField] private SceneOrchestrator sceneOrchestrator;

    private SteamVR_Input_Sources lastSource;
    const float rotationDegrees = 1f;
    private FixedJoint fixedJoint;
    [SerializeField] private Rigidbody controllerRigidbody;
    private Rigidbody rigidbody;



    private bool isBoxAttachingRequested = false;
    private bool isBoxAttached = false;

    private float GetRotationAngle()
    {
        if (lastSource == SteamVR_Input_Sources.RightHand)
        {
            return rotationDegrees;
        }
        else
        {
            return -1f * rotationDegrees;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        fixedJoint = GetComponent<FixedJoint>();
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        if (steamVrActionBoolean.GetStateDown(SteamVR_Input_Sources.RightHand))
        {
            lastSource = SteamVR_Input_Sources.RightHand;
            DetachBox();

        }
        else if (steamVrActionBoolean.GetStateDown(SteamVR_Input_Sources.LeftHand))
        {
            lastSource = SteamVR_Input_Sources.LeftHand;
            if (!steamVrActionBoolean.GetState(SteamVR_Input_Sources.RightHand))
            {
                if (!isBoxAttachingRequested)
                {
                    isBoxAttachingRequested = true;
                    StartCoroutine(WaitAndAttach());
                }
            }
        }

        if (steamVrActionBoolean.GetStateUp(SteamVR_Input_Sources.LeftHand))
        {
            DetachBox();
        }

        if (steamVrActionBoolean.GetState( SteamVR_Input_Sources.RightHand) && steamVrActionBoolean.GetState(SteamVR_Input_Sources.LeftHand))
        {
            transform.Rotate( new Vector3(0,1f,0),GetRotationAngle(), Space.Self);
        }
    }


    private IEnumerator WaitAndAttach()
    {
            yield return new WaitForSeconds(2);
            if (isBoxAttachingRequested)
                AttachBox();
    }

    private void AttachBox()
    {
        isBoxAttached = true;
        sceneOrchestrator.StartGrabbingBox();
        fixedJoint.connectedBody = controllerRigidbody;
        rigidbody.isKinematic = false;
    }

    private void DetachBox()
    {
        rigidbody.isKinematic = true;
        isBoxAttached = false;
        isBoxAttachingRequested = false;
        fixedJoint.connectedBody = null;
        sceneOrchestrator.LeaveGrabbing();
    }
}
