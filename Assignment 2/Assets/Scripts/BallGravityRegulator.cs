﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGravityRegulator : MonoBehaviour
{
    public SceneOrchestrator sceneOrchestrator;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (sceneOrchestrator.getCubeDimension != 0)
        {
            Rigidbody rigidbody = GetComponent<Rigidbody>();

            if (IsOutbound())
            {
                GetComponent<BallStateHolder>().hasLeftTheBox = true;
            }
            else
            {
                GetComponent<BallStateHolder>().hasLeftTheBox = false;
            }
        }
    }

    public bool IsOutbound()
    {
        float distanceToCenter = Vector3.Distance(transform.position, sceneOrchestrator.centerOfTheScene);
        float allowedDistance = (float)(sceneOrchestrator.getCubeDimension * Math.Sqrt(3) / 2);

        return distanceToCenter > allowedDistance;
    }

}
