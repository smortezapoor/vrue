﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Valve.VR;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace Assets.Scripts
{
    class GrabableObject : MonoBehaviour
    {
        private Rigidbody rightHandRigidbody = null;
        private Rigidbody leftHandRigidbody = null;
        [SerializeField]
        float dispositionThreshold = 0.1f;

        public float hitPoints = 100.0F;
        public Collider collider;

        void AdjustLeftHandle()
        {
            
            Vector3 closestPoint = collider.ClosestPointOnBounds(leftHandRigidbody.position);

            if((closestPoint - leftHandHandle.transform.position).magnitude > dispositionThreshold)
            {


                FixedJoint fixedJointL = leftHandHandle.GetComponent<FixedJoint>();
                fixedJointL.connectedBody = fixedJointL.connectedBody;

                leftHandHandle.transform.position = closestPoint;


                FixedJoint fixedJointR = rightHandHandle.GetComponent<FixedJoint>();
                fixedJointR.connectedBody = fixedJointR.connectedBody;
            }
            
        }

        // Start is called before the first frame update
        void Start()
        {
            collider = GetComponent<Collider>();
        }

        // Update is called once per frame
        void Update()
        {
            if (IsLeftHandAttached && IsRightHandAttached)
            {
                AdjustLeftHandle();

            }
        }



        public ConfigurableJoint rightHandHandle = new ConfigurableJoint();
        public ConfigurableJoint leftHandHandle = new ConfigurableJoint();

        public GrabableObject AttachToRightHand(Rigidbody rightHandRigidbody)
        {
            this.rightHandRigidbody = rightHandRigidbody;
            CheckAndFixInitialPosition();
            rightHandHandle.connectedBody = rightHandRigidbody;
            rightHandHandle.connectedAnchor = Vector3.zero;
            rightHandHandle.anchor = Vector3.zero;
            rightHandHandle.GetComponent<Rigidbody>().isKinematic = false;

            return this;
        }

        public GrabableObject AttachToLeftHand(Rigidbody leftHandRigidbody)
        {
            this.leftHandRigidbody = leftHandRigidbody;
            CheckAndFixInitialPosition();
            leftHandHandle.connectedBody = leftHandRigidbody;
            leftHandHandle.connectedAnchor = Vector3.zero;
            leftHandHandle.anchor = Vector3.zero;
            leftHandHandle.GetComponent<Rigidbody>().isKinematic = false;



            return this;
        }

        private void CheckAndFixInitialPosition()
        {
            if (IsRightHandAttached && IsLeftHandAttached)
            {
                Vector3 stickCorrectDirection =
                    (leftHandRigidbody.transform.position - rightHandRigidbody.transform.position);

                this.transform.parent.transform.Translate(stickCorrectDirection);
            }
        }

        public GrabableObject DetachFromRightHand()
        {
            rightHandHandle.GetComponent<Rigidbody>().isKinematic = true;

            rightHandHandle.connectedBody = null;
            return this;
        }
        public GrabableObject DetachFromLeftHand()
        {
            leftHandHandle.GetComponent<Rigidbody>().isKinematic = true;

            leftHandHandle.connectedBody = null;
            return this;
        }
        public bool IsRightHandAttached => rightHandHandle.connectedBody != null;
        public bool IsLeftHandAttached => leftHandHandle.connectedBody != null;


    }
}
