﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class CueStickHandler : MonoBehaviour
{
    public SteamVR_Action_Boolean steamVrActionBoolean = null;


    private GrabableObject grabableObject = null;
    private SteamVR_Behaviour_Pose steamVrBehaviourPose = null;

    public Rigidbody rightHandRigidbody;
    public Rigidbody leftHandRigidbody;


    public SteamVR_Action_Vibration steamVrActionVibration;

    // Start is called before the first frame update
    void Start()
    {
        steamVrBehaviourPose = GetComponent<SteamVR_Behaviour_Pose>();
    }

    

    // Update is called once per frame
    void Update()
    {
        if (steamVrActionBoolean.GetStateDown(steamVrBehaviourPose.inputSource))
        {
            if (grabableObject != null)
            {
                if (!grabableObject.IsRightHandAttached)
                {
                    grabableObject
                        .AttachToRightHand(rightHandRigidbody)
                    .AttachToLeftHand(leftHandRigidbody);
                    Pulse();

                }
                else
                {
                    grabableObject
                        .DetachFromRightHand()
                        .DetachFromLeftHand();
                    Pulse();



                }
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (grabableObject == null)
        {
            GrabableObject _grabableObject = other.GetComponentInParent<GrabableObject>();
            if (_grabableObject != null && !_grabableObject.IsRightHandAttached)
            {
                Pulse(true);
            }
        }
    }

    private void Pulse()
    {
        Pulse(false);
    }

    private void Pulse(bool half)
    {
        steamVrActionVibration.Execute(0,half? 0.025f : 0.05f, 75f, 0.5f, SteamVR_Input_Sources.Any);

    }

    private void OnTriggerStay(Collider other)
    {
        grabableObject = other.GetComponentInParent<GrabableObject>();
    }

    private void OnTriggerExit(Collider other)
    {
        grabableObject = null;
    }


}
