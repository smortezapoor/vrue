﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixJointMassScaleRegulator : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        FixedJoint fixedJoint = this.GetComponent<FixedJoint>();
        if (fixedJoint.connectedBody != null)
        {
            fixedJoint.massScale = 1f / fixedJoint.connectedBody.mass;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
