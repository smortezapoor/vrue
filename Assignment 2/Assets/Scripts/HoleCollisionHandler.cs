﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class HoleCollisionHandler : MonoBehaviour
{
     float holeDiameter = 0;
     private const float speed = 0.1f;

    public void SetHoleDiameter(float holeDiameter)
    {
        this.holeDiameter = holeDiameter;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(holeDiameter!= 0)
        {
            if (other.gameObject.tag == "Ball")
            {
                other.gameObject.layer = LayerMask.NameToLayer("PTLayer");
                if (!other.GetComponent<BallGravityRegulator>().IsOutbound())
                {
                    float step = speed * Time.deltaTime; // calculate distance to move
                    other.transform.position = Vector3.MoveTowards(other.transform.position, transform.position,
                        holeDiameter);
                }
                
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
    }


}
