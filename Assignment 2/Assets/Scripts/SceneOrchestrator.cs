﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Valve.VR;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class SceneOrchestrator : MonoBehaviour
{
    #region Variables

    const float cubeDimension = 1f;
    const float ballDiameter = 0.1f;
    const float holeDiameter = 0.15f;
    const float holeDiameter_epsilon = 0.05f;
    const float wallThickness = 0.01f;
    const float offsetFromGround = 2f;
    const float epsilon = 0.020f;

    public float getCubeDimension => cubeDimension;

    [SerializeField] private Material boxFaceMaterial;
    [SerializeField] private Material holeMaterial;

    Vector3[] cubeTargetPosition = new Vector3[6]
    {
        new Vector3(0, offsetFromGround, 0),
        new Vector3(0, offsetFromGround + cubeDimension, 0),
        new Vector3(0 - (cubeDimension / 2f), offsetFromGround + (cubeDimension / 2f), 0),
        new Vector3(0, offsetFromGround + (cubeDimension / 2f), 0 + (cubeDimension / 2f)),
        new Vector3(0 + (cubeDimension / 2f), offsetFromGround + (cubeDimension / 2f), 0),
        new Vector3(0, offsetFromGround + (cubeDimension / 2f), 0 - (cubeDimension / 2f))
    };

    private Vector3[] holeTargetPosition = new Vector3[8]
    {
        new Vector3(cubeDimension / 2f, offsetFromGround, cubeDimension / 2f),
        new Vector3(cubeDimension / 2f, offsetFromGround, -cubeDimension / 2f),
        new Vector3(-cubeDimension / 2f, offsetFromGround, cubeDimension / 2f),
        new Vector3(-cubeDimension / 2f, offsetFromGround, -cubeDimension / 2f),
        new Vector3(cubeDimension / 2f, offsetFromGround + cubeDimension, cubeDimension / 2f),
        new Vector3(cubeDimension / 2f, offsetFromGround + cubeDimension, -cubeDimension / 2f),
        new Vector3(-cubeDimension / 2f, offsetFromGround + cubeDimension, cubeDimension / 2f),
        new Vector3(-cubeDimension / 2f, offsetFromGround + cubeDimension, -cubeDimension / 2f),
    };

    public Vector3 centerOfTheBox => transform.position;
    public Vector3 centerOfTheScene = new Vector3(0, offsetFromGround + (cubeDimension / 2f), 0);

    GameObject[] cubes = new GameObject[6];
    GameObject[] holes = new GameObject[8];
    GameObject[] balls = new GameObject[14];
    GameObject cueBall;
    [SerializeField]
    GameObject cueStickSet;

    [SerializeField] private Quaternion cueStickRotation = Quaternion.AngleAxis(-90f, new Vector3(1, 0,0));
    [SerializeField] private Vector3 cueStickRelativePosition = new Vector3(4.16f,0,0.39f);

    [SerializeField] GameObject p3dPoolSetContainer;

    #endregion


    // Start is called before the first frame update
    void Start()
    {
        this.transform.parent.position = centerOfTheScene;
        p3dPoolSetContainer.transform.position = Vector3.zero;
        GameObject poolSceneGameObject = new GameObject("3dPoolSet");
        poolSceneGameObject.transform.parent = p3dPoolSetContainer.transform;
        poolSceneGameObject.transform.localPosition = Vector3.zero;
        CreateGamingBox(poolSceneGameObject);
        CreateBalls(poolSceneGameObject);
        CreateCueStick(this.gameObject.transform.parent.gameObject);
        IgnoreBoxAndCueStickCollision();
    }

    private void IgnoreBoxAndCueStickCollision()
    {
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("BoxLayer"), LayerMask.NameToLayer("PTLayer"));
    }

    #region CueStick

    private void CreateCueStick(GameObject parentGameObject)
    {
        GameObject cueStickObject = Instantiate(cueStickSet, cueStickRelativePosition + centerOfTheScene, Quaternion.identity);
        cueStickObject.name = "CueStickSet";
        cueStickObject.transform.parent = parentGameObject.transform;
        cueStickObject.transform.eulerAngles = new Vector3(
            cueStickObject.transform.eulerAngles.x,
            cueStickObject.transform.eulerAngles.y -90,
            cueStickObject.transform.eulerAngles.z
        );
    }

    #endregion

    #region Balls

    private void CreateBalls(GameObject parentGameObject)
    {
        GameObject ballParentGameObject = new GameObject();
        ballParentGameObject.name = "Balls";
        ballParentGameObject.transform.parent = parentGameObject.transform;
        for (int i = 0; i < 14; i++)
        {
            balls[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            balls[i].transform.parent = ballParentGameObject.transform;
            balls[i].name = $"Ball ({i})";
            balls[i].transform.localScale = new Vector3(ballDiameter, ballDiameter, ballDiameter);
            balls[i].transform.position = GetInitialBallPosition(i);
            balls[i].AddComponent<BallStateHolder>();
            AssignMaterialToTheBall(ref balls[i], i);
            HandleBallRigidBody(ref balls[i]);
            HandleBallGravityRegulator(ref balls[i]);
            balls[i].transform.tag = "Ball";
        }

        //Cue
        cueBall = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        cueBall.transform.parent = ballParentGameObject.transform;
        cueBall.name = $"Ball (Cue ball)";
        cueBall.transform.localScale = new Vector3(ballDiameter, ballDiameter, ballDiameter);
        cueBall.transform.position = GetInitialCueBallPosition();
        AssignMaterialToTheBall(ref cueBall, null);
        HandleBallRigidBody(ref cueBall);

    }

    private void HandleBallGravityRegulator(ref GameObject ball)
    {
        ball.AddComponent<BallGravityRegulator>().sceneOrchestrator = this;
    }

    private Vector3 GetInitialCueBallPosition()
    {
        return new Vector3(centerOfTheScene.x + cubeDimension / 4f, centerOfTheScene.y,
            centerOfTheScene.z);
    }

    private void HandleBallRigidBody(ref GameObject ball)
    {
        Rigidbody gameObjectsRigidBody = ball.AddComponent<Rigidbody>(); // Add the rigidbody.
        gameObjectsRigidBody.mass = 0.0005f;
        gameObjectsRigidBody.useGravity = false;
    }

    Vector3 GetInitialBallPosition(int ballNumber)
    {
        Tuple<int, int, int> ballLocation = GetBallLocationInHierarchy(ballNumber);
        float elevation = ballDiameter * (float) Math.Cos(Math.PI / 3.0) + epsilon;
        float y = centerOfTheScene.y + elevation - (ballLocation.Item1 * elevation);
        float x = centerOfTheScene.x - (ballDiameter * ballLocation.Item1 / 2f) + (ballLocation.Item2 * ballDiameter);
        float z = centerOfTheScene.z - (ballDiameter * ballLocation.Item1 / 2f) + (ballLocation.Item3 * ballDiameter);

        return new Vector3(x, y, z);
    }

    private Tuple<int, int, int> GetBallLocationInHierarchy(int ballNumber)
    {
        int k = 0;
        int levelRemainingBalls = 0;
        if (ballNumber < 1)
        {
            k = 0;
        }
        else if (ballNumber < 5)
        {
            k = 1;
            levelRemainingBalls = ballNumber - 1;
        }
        else
        {
            k = 2;
            levelRemainingBalls = ballNumber - 5;
        }

        return new Tuple<int, int, int>(k, (int) (levelRemainingBalls / (k + 1)), levelRemainingBalls % (k + 1));

    }

    public Material[] ballsMaterials;

    private void AssignMaterialToTheBall(ref GameObject ball, int? i)
    {

        var rend = ball.GetComponent<Renderer>();
        rend.material = ballsMaterials[i ?? 14];
    }

    #endregion

    #region Gaming box

    private void CreateGamingBox(GameObject parentGameObject)
    {
        GameObject boxParentGameObject = new GameObject();
        boxParentGameObject.transform.parent = parentGameObject.transform;
        boxParentGameObject.name = "GameBox";
        boxParentGameObject.layer = LayerMask.NameToLayer("BoxLayer");
        AddBoxFaces(boxParentGameObject);
        AddBoxHoles(boxParentGameObject);
    }

    private void AddBoxHoles(GameObject boxParentGameObject)
    {
        GameObject holeParentGameObject = new GameObject();
        holeParentGameObject.name = "Holes";
        holeParentGameObject.transform.parent = boxParentGameObject.transform;
        holeParentGameObject.layer = LayerMask.NameToLayer("BoxLayer");
        for (int i = 0; i < 8; i++)
        {
            holes[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            holes[i].transform.parent = holeParentGameObject.transform;
            holes[i].name = $"Hole ({i})";
            holes[i].transform.localScale = new Vector3(holeDiameter, holeDiameter, holeDiameter);
            holes[i].transform.position = holeTargetPosition[i];
            AssignMaterialToTheHole(ref holes[i]);
            //HandleHoleRigidBody(ref holes[i]);
            HandleHoleCollider(ref holes[i]);
            holes[i].layer = LayerMask.NameToLayer("BoxLayer");

        }
    }

    private void HandleHoleRigidBody(ref GameObject hole)
    {
        hole.AddComponent<Rigidbody>();
        Rigidbody rigidbody = hole.GetComponent<Rigidbody>();
        rigidbody.isKinematic = true;
        rigidbody.useGravity = false;
    }

    private void HandleHoleCollider(ref GameObject hole)
    {
        hole.AddComponent<HoleCollisionHandler>();
        hole.GetComponent<HoleCollisionHandler>().SetHoleDiameter(holeDiameter);
        SphereCollider collider = hole.GetComponent<SphereCollider>();
        collider.center = Vector3.zero;
        collider.radius = holeDiameter * 4 + epsilon;
        collider.isTrigger = true;
    }

    private void AssignMaterialToTheHole(ref GameObject hole)
    {
        var rend = hole.GetComponent<Renderer>();
        rend.material = holeMaterial;
    }

    private void AddBoxFaces(GameObject boxParentGameObject)
    {
        //0
        cubes[0] = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubes[0].transform.localScale = new Vector3(cubeDimension, wallThickness, cubeDimension);
        cubes[0].transform.position = cubeTargetPosition[0];
        cubes[0].transform.parent = boxParentGameObject.transform;
        HandleBoxFaceRigidBody(ref cubes[0]);
        cubes[0].name = $"Face (0)";
        AssignMaterialToTheCube(ref cubes[0]);


        //1
        cubes[1] = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubes[1].transform.localScale = new Vector3(cubeDimension, wallThickness, cubeDimension);
        cubes[1].transform.position = cubeTargetPosition[1];
        cubes[1].transform.parent = boxParentGameObject.transform;
        HandleBoxFaceRigidBody(ref cubes[1]);
        cubes[1].name = $"Face (1)";
        AssignMaterialToTheCube(ref cubes[1]);

        //2
        cubes[2] = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubes[2].transform.localScale = new Vector3(wallThickness, cubeDimension, cubeDimension);
        cubes[2].transform.position = cubeTargetPosition[2];
        cubes[2].transform.parent = boxParentGameObject.transform;
        HandleBoxFaceRigidBody(ref cubes[2]);
        cubes[2].name = $"Face (2)";
        AssignMaterialToTheCube(ref cubes[2]);

        //3
        cubes[3] = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubes[3].transform.localScale = new Vector3(cubeDimension, cubeDimension, wallThickness);
        cubes[3].transform.position = cubeTargetPosition[3];
        cubes[3].transform.parent = boxParentGameObject.transform;
        HandleBoxFaceRigidBody(ref cubes[3]);
        cubes[3].name = $"Face (3)";
        AssignMaterialToTheCube(ref cubes[3]);

        //4
        cubes[4] = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubes[4].transform.localScale = new Vector3(wallThickness, cubeDimension, cubeDimension);
        cubes[4].transform.position = cubeTargetPosition[4];
        cubes[4].transform.parent = boxParentGameObject.transform;
        HandleBoxFaceRigidBody(ref cubes[4]);
        cubes[4].name = $"Face (4)";
        AssignMaterialToTheCube(ref cubes[4]);

        //5
        cubes[5] = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubes[5].transform.localScale = new Vector3(cubeDimension, cubeDimension, wallThickness);
        cubes[5].transform.position = cubeTargetPosition[5];
        cubes[5].transform.parent = boxParentGameObject.transform;
        HandleBoxFaceRigidBody(ref cubes[5]);
        cubes[5].name = $"Face (5)";
        AssignMaterialToTheCube(ref cubes[5]);


        foreach (GameObject cube in cubes)
        {
            cube.layer = LayerMask.NameToLayer("BoxLayer");
        }
    }

    private void HandleBoxFaceRigidBody(ref GameObject cube)
    {
        Rigidbody gameObjectsRigidBody = cube.AddComponent<Rigidbody>();
        gameObjectsRigidBody.useGravity = false;
        gameObjectsRigidBody.isKinematic = true;
        gameObjectsRigidBody.mass = 0.0100f;
    }

    private void AssignMaterialToTheCube(ref GameObject cube)
    {

        var rend = cube.GetComponent<Renderer>();
        rend.material = boxFaceMaterial;
    }

    #endregion

    #region Grabbing

    public void StartGrabbingBox()
    {
        foreach (GameObject ball in balls)
        {
            Rigidbody rigidBody = ball.GetComponent<Rigidbody>();
            if (!ball.GetComponent<BallStateHolder>().hasLeftTheBox)
                rigidBody.isKinematic = true;
        }
        cueBall.GetComponent<Rigidbody>().isKinematic = true;
    }

    public void LeaveGrabbing()
    {
        foreach (GameObject ball in balls)
        {
            Rigidbody rigidBody = ball.GetComponent<Rigidbody>();
            rigidBody.isKinematic = false;
        }

        cueBall.GetComponent<Rigidbody>().isKinematic = false;
    }
    #endregion

}
