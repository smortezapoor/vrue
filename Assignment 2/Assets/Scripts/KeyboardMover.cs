﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMover : MonoBehaviour
{
    private float speed = 2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    [SerializeField]
    private GameObject targetOrientationObject = null;

    // Update is called once per frame
    void Update()
    {
        if (targetOrientationObject == null)
            targetOrientationObject = GetComponent<GameObject>();

        Quaternion rotation = targetOrientationObject.transform.localRotation;
        transform.localRotation = rotation;
        targetOrientationObject.transform.localRotation = rotation;

        //set the orientation to the orientation of the main

        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        float scrollInput = Input.mouseScrollDelta.y;

        Vector3 direction = new Vector3(horizontalInput,verticalInput, scrollInput);
        
        transform.Translate(direction * speed * Time.deltaTime);

    }
}
