﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Interfaces;

public class RaycastController : MonoBehaviour
{

    #region External references
    [SerializeField]
    private CapsuleHand handModel = null;

    [SerializeField]
    private ExtendedFingerDetector pistolExtendedFingerDetector;
    [SerializeField]
    private ExtendedFingerDetector pistolObjectSelectionDetector;
    #endregion

    #region Public API
    public bool pistolGestureDetected => (ac_pistolExtendedFingerDetector != null && ac_pistolExtendedFingerDetector.IsActive);

    public bool pistolObjectSelectionGestureDetected => (ac_pistolObjectSelectionDetector != null && ac_pistolObjectSelectionDetector.IsActive);

    #endregion

    #region Accessors
    private CapsuleHand ac_handModel
    {
        get
        {
            if (handModel == null) handModel = GetComponent<CapsuleHand>();
            return handModel;
        }
    }

    private ExtendedFingerDetector ac_pistolExtendedFingerDetector
    {
        get
        {
            if (pistolExtendedFingerDetector == null) pistolExtendedFingerDetector = GetComponent<ExtendedFingerDetector>();
            return pistolExtendedFingerDetector;
        }
    }

    private ExtendedFingerDetector ac_pistolObjectSelectionDetector
    {
        get
        {
            if (pistolObjectSelectionDetector == null) pistolObjectSelectionDetector = GetComponent<ExtendedFingerDetector>();
            return pistolObjectSelectionDetector;
        }
    }

    public GameObject ac_lineRendererGameObject
    {
        get
        {
            //render the linerenderer to show the raycast laser
            if (lineRendererGameObject == null) lineRendererGameObject = gameObject.transform.Find("RaycastLineRenderer").gameObject;
            return lineRendererGameObject;
        }
    }

    #endregion

    #region Internal fields
    private int targetFinger = (int)Finger.FingerType.TYPE_INDEX;
    private GameObject lineRendererGameObject = null;
    private float maxRaycastLaserDistance = 10;
    private IPickable lastHitIPickable;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        ac_lineRendererGameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        HandleRaycast();
    }

    private void HandleRaycast()
    {
        bool thereIsARaycastHit = false;

        if (pistolGestureDetected || pistolObjectSelectionGestureDetected)
        {
            //if the hand model is present
            if (ac_handModel.GetLeapHand() != null)
            {
                //show the raycast line renderer
                ac_lineRendererGameObject.SetActive(true);

                //initiate the raycast
                thereIsARaycastHit = Physics.Raycast(
                    ac_handModel.GetLeapHand().Fingers[targetFinger].TipPosition.ToVector3(),
                    ac_handModel.GetLeapHand().Fingers[targetFinger].Direction.ToVector3(), out RaycastHit raycastHit,
                    Mathf.Infinity);

                if (thereIsARaycastHit)
                {
                    //cut the raycast when there is a hit
                   maxRaycastLaserDistance = raycastHit.distance * 1.1f;

                   //handle raycast hit
                   HandleRaycastHit(raycastHit);

                   //mark that there is a raycast hit
                   thereIsARaycastHit = true;
                }



                //rendering raycast
                RenderRaycastLaser(ac_handModel.GetLeapHand().Fingers[targetFinger].TipPosition.ToVector3(),
                    ac_handModel.GetLeapHand().Fingers[targetFinger].Direction.ToVector3(), maxRaycastLaserDistance);

            }
        }
        else
        {
            ac_lineRendererGameObject.SetActive(false);
        }

        if (!thereIsARaycastHit)
        {
            ReleaseLastRaycastHit();
        }
    }

    private void ReleaseLastRaycastHit()
    {
        lastHitIPickable?.LeaveAlone();
        lastHitIPickable = null;
    }

    private void HandleRaycastHit(RaycastHit raycastHit)
    {
        if (raycastHit.transform.tag == "Pickable" && raycastHit.collider.GetComponent<CubeBehaviorControl>() is IPickable)
        {
            IPickable newCubeBehaviorControl = raycastHit.collider.GetComponent<CubeBehaviorControl>();
            if (lastHitIPickable != null && lastHitIPickable != newCubeBehaviorControl)
            {
                ReleaseLastRaycastHit();
            }
            lastHitIPickable = newCubeBehaviorControl;
            //here decide

            if (pistolObjectSelectionGestureDetected)
            {
                FixedJoint fixedJoint = ac_lineRendererGameObject.GetComponent<FixedJoint>();
                fixedJoint.connectedAnchor = raycastHit.point;
                lastHitIPickable.StartPicking(fixedJoint);

            }
            else
            {
                lastHitIPickable.MarkAsReadyToPick();
            }
        }
        else
        {
            ReleaseLastRaycastHit();
        }
    }


    private void RenderRaycastLaser(Vector3 targetPosition, Vector3 direction, float length)
    {
        Vector3 endPosition = targetPosition + (length * direction);
        ac_lineRendererGameObject.GetComponent<LineRenderer>().SetPositions(new Vector3[] { targetPosition, endPosition });
    }
}
