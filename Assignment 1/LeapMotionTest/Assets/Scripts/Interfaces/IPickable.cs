﻿using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public interface IPickable
    {
        void MarkAsReadyToPick();
        void StartPicking(FixedJoint fixedJointToAttach);
        void LeaveAlone();
        void StartCreating();
    }
}