﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Assets.Scripts.Interfaces;
using Leap.Unity;
using UnityEngine;

public class CustomPinchDetection : MonoBehaviour
{
    public PinchDetector[] pinchDetectors;

    public GameObject cubePrefab;

    private GameObject tempGameObject = null;
    private bool objectAttached = false;







    #region Constants

    private float maxScale = 0.30f;
    private float minScale = 0.04f;

    #endregion


    // Start is called before the first frame update
    void Start()
    {
    }


    // Update is called once per frame
    void Update()
    {


        if (objectAttached)
        {
            //scale
            float _scale = Mathf.Abs(Vector3.Distance(pinchDetectors[0].Position, pinchDetectors[1].Position));
            _scale = Math.Min(maxScale, Math.Max(_scale, minScale));
            tempGameObject.transform.localScale = new Vector3(_scale, _scale, _scale);

            //size
            Vector3 pinchesMidPoint = (pinchDetectors[0].Position + pinchDetectors[1].Position) / 2;
            tempGameObject.transform.localPosition = pinchesMidPoint;

            //orientation
            float pinchesHorizontalDistance = (pinchDetectors[0].Position.x - pinchDetectors[1].Position.x);
            float pinchesVerticalDistance = (pinchDetectors[0].Position.y - pinchDetectors[1].Position.y);
            float angleOfTheCube =
                (float) (Math.Atan(pinchesVerticalDistance / pinchesHorizontalDistance) * (180.0 / Math.PI));

            tempGameObject.transform.eulerAngles = new Vector3(0, 0, angleOfTheCube);
        }
    }


    public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
    {
        return Mathf.Atan2(
                   Vector3.Dot(n, Vector3.Cross(v1, v2)),
                   Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    public void spawnCube()
    {
        tempGameObject = Instantiate(cubePrefab, (pinchDetectors[0].Position + pinchDetectors[1].Position) / 2,
            Quaternion.identity);
        if (tempGameObject.GetComponent<CubeBehaviorControl>() is IPickable)
        {
            tempGameObject.GetComponent<CubeBehaviorControl>().StartCreating();
        }

        objectAttached = true;
    }

    public void leaveCube()
    {
        if (tempGameObject != null)
        {
            if (tempGameObject.GetComponent<CubeBehaviorControl>() is IPickable)
            {
                tempGameObject.GetComponent<CubeBehaviorControl>().LeaveAlone();
            }

        }

        objectAttached = false;
    }

}
