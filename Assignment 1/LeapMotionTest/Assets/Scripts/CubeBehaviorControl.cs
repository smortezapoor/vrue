﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.Interfaces;
using Leap.Unity;
using UnityEditor;
using UnityEngine;

public class CubeBehaviorControl : MonoBehaviour, IPickable
{
    [SerializeField]
    private Material materialNormal;
    [SerializeField]
    private Material materialCreating;
    [SerializeField]
    private Material materialReadyToPick;
    [SerializeField]
    private Material materialPicked;


    MeshRenderer __meshRenderer;
    public MeshRenderer ac_meshRenderer {
        get
        {
            if (__meshRenderer == null) __meshRenderer = GetComponent<MeshRenderer>();
            return __meshRenderer;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }


    // Update is called once per frame
    void Update()
    {
    }


    private FixedJoint joint = null;

    public void MarkAsReadyToPick()
    {
        ac_meshRenderer.material = materialReadyToPick;
    }

    public void StartPicking(FixedJoint fixedJointToAttach)
    {
        if (joint == null)
        {
            ac_meshRenderer.material = materialPicked;
            fixedJointToAttach.connectedBody = MakeItInteractable();
            joint = fixedJointToAttach;
        }
    }



    public void LeaveAlone()
    {
        ac_meshRenderer.material = materialNormal;
        ApplyForces();
    }
    private Rigidbody MakeItInteractable()
    {
        Rigidbody rigidBody = this.GetComponent<Rigidbody>();
        rigidBody.useGravity = false;
        rigidBody.isKinematic = false;
        return rigidBody;
    }

    private Rigidbody ApplyForces()
    {
        Rigidbody rigidBody = this.GetComponent<Rigidbody>();
        rigidBody.useGravity = true;
        rigidBody.isKinematic = false;
        if (joint != null) joint.connectedBody = null;
        joint = null;
        return rigidBody;
    }

    public void StartCreating()
    {
        ac_meshRenderer.material = materialCreating;
        MakeItInteractable();
    }
}
